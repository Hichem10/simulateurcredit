﻿$('#montant').ionRangeSlider({
    type: "single",
    min: 0,
    max: 40000,
    grid: true,
    onFinish: function (data) {
        
        var dataResult = JSON.stringify({ 'montant': parseInt($('#montant').val()), 'duree': parseInt($('#duree').val()) })
        $.ajax({
            type: "POST",
            url: 'Home/SimulerCredit',
            data: dataResult,
            datatype: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                let my_range = $("#mens").data("ionRangeSlider");
                my_range.update({
                    from: parseInt(response),
                });
            }
        })
    }
});


$('#duree').ionRangeSlider({
    type: "single",
    min: 0,
    max: 84,
    grid: true,
    onFinish: function (data) {
        
        var dataResult = JSON.stringify({ 'montant': parseInt($('#montant').val()), 'duree': parseInt($('#duree').val()) })
        $.ajax({
            type: "POST",
            url: 'Home/SimulerCredit',
            data: dataResult,
            datatype: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (response) {
                let my_range = $("#mens").data("ionRangeSlider");
                my_range.update({
                    from: parseInt(response),
                });
            }
        })
    }


});


$('#mens').ionRangeSlider({
    type: "single",
    min: 0,
    max: 8000,
    grid: true,

});


$('#submit-button').on("click", function () {
    $('#montant-name').val($('#montant').val());
    $('#duree-name').val($('#duree').val());
    $('#mens-name').val($('#mens').val());
});
