﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimulateurCredit.Models
{
    public class Simulation
    {
        public int Montant { get; set; }
        public int Duree { get; set; }
    }
}
